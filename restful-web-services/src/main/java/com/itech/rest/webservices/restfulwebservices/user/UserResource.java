package com.itech.rest.webservices.restfulwebservices.user;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
public class UserResource {

	@Autowired
	private UserDaoService userDaoService;

	/**
	 * RETRIEVE ALL USERS
	 * 
	 * @return users
	 */
	@GetMapping("/users")
	public List<User> retrieveAllUsers() {
		return userDaoService.findAll();
	}

	/**
	 * RETRIEVE USER
	 * 
	 * @param id
	 * @return user by id
	 */
	@GetMapping("/users/{id}")
	public User retrieveUser(@PathVariable int id) {
		User user = userDaoService.findOne(id);

		if (user == null) {
			throw new UserNotFoundException("id-" + id);
		}

		return user;
	}

	/**
	 * CREATE A NEW USER
	 * 
	 * @param user
	 */
	@PostMapping("/users")
	public ResponseEntity<User> createUser(@Valid @RequestBody User user) {

		User savedUser = userDaoService.save(user);

		// This will return current request URI or URI of newly created user
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedUser.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}

	/**
	 * DELETES A USER BY ID
	 * 
	 * @param id
	 */
	@DeleteMapping("/users/{id}")
	public void deleteUser(@PathVariable int id) {

		User user = userDaoService.deleteUserById(id);

		if (user.getId() == null) {
			throw new UserNotFoundException("id-" + id);
		}
	}
}
