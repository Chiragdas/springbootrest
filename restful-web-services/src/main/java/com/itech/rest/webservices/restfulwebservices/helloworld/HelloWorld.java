package com.itech.rest.webservices.restfulwebservices.helloworld;

public class HelloWorld {

	private String message;
	
	public HelloWorld(String message) {
		// TODO Auto-generated constructor stub
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "HelloWorldBean [message=" + message + "]";
	}

}
