package com.itech.rest.webservices.restfulwebservices.posts;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PostResource {

	@Autowired
	private PostDaoService postDaoService;
	
	/**
	 * RETRIEVES ALL POST OF USER
	 * @return
	 */
	@GetMapping("/users/{id}/posts")
	public List<Post> retrieveAllPost(){
		return null;
	}
	
	/**
	 * CREATE POST FOR USER
	 * @param post
	 * @return
	 */
	@PostMapping("users/{id}/posts")
	public Post createPost(@RequestBody Post post) {
		return null;
	}
	
}
