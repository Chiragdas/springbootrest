package com.itech.rest.webservices.restfulwebservices.helloworld;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.itech.rest.webservices.restfulwebservices.helloworld.HelloWorld;

@RestController("hello")
public class HelloWorldController {

	/**
	 * THIS METHOD RETURN STRING AS A RESPONSE
	 * @return string
	 */
	@GetMapping(path = "/hello-world")
	public String helloWorld() {
		return "Hello World";
	}

	/**
	 * THIS METHOD RETURNS BEAN AS A RESPONSE
	 * @return bean
	 */
	@GetMapping(path = "/hello-world-bean")
	public HelloWorld helloWorldBean() {
		return new HelloWorld("Hello-World");
	}
		
	/**
	 * THIS METHOD SHOWS THE USAGE OF PATH VARIABLE
	 * @param name
	 * @return bean
	 */
	@GetMapping(path = "/hello-world/path-variable/{name}")
	public HelloWorld helloWorldPathVariable(@PathVariable String name) {
		return new HelloWorld(String.format("Hello %s", name));
	}
}
