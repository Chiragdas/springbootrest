# SpringBootRest

This project is created with the intent to explore restful web services using Spring Boot.

Project Overview:
-----------------

- We would be creating a Basic Social Media Application in which there will be a list of users and these users can deal with their posts in application.

Development Steps:
-------------------

STEP 1: CREATE A SPRING BOOT WEB PROJECT

STEP 2: IDENTIFY THE RESOURCES
        
        1) UserResource
        2) PostResource

STEP 3: CREATE A USER RESOURCE

        USERRESOURCE:
        -------------

        - RETRIEVE ALL USERS                - GET    /users
        - CREATE A USER                     - POST   /users
        - RETRIEVE ONE USER                 - GET    /users/{id}
        - DELETE A USER                     - DELETE /users/{id}

STEP 4: CREATE A POST RESOURCE

        POSTRESOURCE:
        -------------

        - RETRIEVE ALL POSTS FOR A USER     - GET    /users/{id}/posts
        - CREATE A POST FOR A USER          - POST   /users/{id}/posts
        - RETRIEVE DETAILS OF A POST        - GET    /users/{id}/posts/{post_id}